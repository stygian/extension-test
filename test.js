function isFullyInViewport (el) {
    var rect = el.getBoundingClientRect();

    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
}

function isPartiallyInViewport(el) {
    const rect = el.getBoundingClientRect();
    const isWithinHorizontal = rect.left >= 0 && rect.right <= (window.innerWidth || document.documentElement.clientWidth);
    const isBelowBottom = rect.top > (window.innerHeight || document.documentElement.clientHeight);
    const isAboveTop = rect.bottom < 0;

    if (!isWithinHorizontal) {
        return false;
    }

    return !(isBelowBottom || isAboveTop);
}

let elements = [];

function parse(element) {
    const skippedElements = [ 'SCRIPT', 'NOSCRIPT', 'IMG', 'INPUT' ];

    // TODO: skip invisible elements
    if (skippedElements.includes(element.tagName)) {
        return;
    }

    if (isFullyInViewport(element)) {
        elements.push(element);
        return;
    }

    let children = element.children;
    if ((!children || !children.length) && isPartiallyInViewport(element)) {
        elements.push(element);
        return;
    }

    for (let el of children) {
        parse(el);
    }
}

parse(document.querySelector('body'));

console.log(elements);
